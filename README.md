**_Getting Started_**


1. Create a repo on [Gitlab](gitlab.com) and copy your SSH remote, it should look something like `git@gitlab.com:jonathan-kosgei/godeps-main.git`

2. Set up your repo locally; this is a sample app using nginx
```
    # tar -xvf gitlab-ci.tar.gz; get the tar file from your admin
    # cd ci
    # git init
    # git add remote origin <ssh remote> # replace ssh remote with the value you copied from gitlab
```
3. Next we need to setup authentication for gcloud, this will allow us the build to run kubectl commands remotely to Google Container Engine,
as well as allow us to push images to Google's private container registry (GCR) , there're 2 ways to authenticate gcloud; via auth token
or via serviceaccount, we're going to use a serviceaccount.

The following excerpt from the CircleCI docs explains how to setup authentication for gcloud
>Creating and downloading the JSON Service Account
>To create and download a Service Account you can visit https://console.developers.google.com/apis/credentials/serviceaccountkey?project=_, or just go to go to https://console.developers.google.com/, and then on the left-hand menu click ‘API Manager’, then ‘Credentials’, then ‘New credentials’, >then select ‘Service account key’. Create a new service account, give it a name, make sure it’s in the JSON format, then click ‘Create’. This will download the Service Account JSON file.
>
>Please keep in mind that the Service Account is a credential that can be used to interact with the project on your behalf, so keep it secret along with any other credentials.

Following the above when you hit create the key will be downloaded automatically, take care of it as it is the only copy of the key that exists
after the key has been downloaded open a terminal and run, 
```
    # cd `~/Downloads`  # this assumes you're on linux and ~/Downloads is your default download location, adjust accordingly
    # base64 <your-service-account.json> 
    A large block of text will be outputted to the terminal, make sure to copy all of it
```

4. Replace `service-account` in your .gitlab-ci.yml with the result of that command, note the quotes
```
      variables:
        GCLOUD_SERVICE_KEY: 'lots or random numbers and letters' 
```
This sets the GCLOUD_SERVICE_KEY env variable that will be used during the build to authenticate gcloud commands

Then to trigger your first build run

    # git add .
    # git commit -m 'Init'
    # git push origin master # running this is all you need to trigger the build

Done!

Upon running git push, the ci will build a new docker image adding the new code that has been pushed, create a kubernetes deployment if one
does not exist on GKE and update the deployment if it does exist with the new latest image version that has been pushed to GCR

The build runs automatically, you can go to 'Pipelines' in your project and see if there's a green 'passed' button next to your last commit.

To manually test the build

    1. Go to 'Pipelines' in your project
    2. You'll see either a red 'pending' or blue 'running' button next to your latest commit
    3. You can click on the button to watch the run of the build

**_YAML Variables_**
1. To edit the ingress path edit the following block in ci/nginx-ci-ingress.yml
```
            backend: 
              serviceName: nginx-ci
              servicePort: 80
            path: /nginx-ci/
```
Replacing path with your desired path; the path must also match a directory in the container under `/usr/share/nginx/html` so also edit
```
RUN mkdir -p /usr/share/nginx/html/nginx-ci
```
The above line in `docker/Dockerfile`
2. To change the service name or selectors edit the metadata block in `ci-yaml/nginx-ci-service.yml`
```
metadata:
  labels:
    name: nginx-ci
  name: nginx-ci
```
Make sure the service name you set here is reflected in `ci-yaml/nginx-ci-ingress.yml` under `serviceName`
```
            backend: 
              serviceName: nginx-ci
              servicePort: 80
            path: /nginx-ci/
```
3. To edit the ports exposed change the `ports` block in `ci-yaml/nginx-ci-deployment.yml`
```
        ports:
        - containerPort: 80
```
4. To change the selector for the service i.e. the label we set on our pods so that our service knows which pods are under it edit
```
    metadata:
      labels:
        name: nginx-ci
```
Under `ci-yaml/nginx-ci-deployment.yml`